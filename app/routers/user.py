from fastapi import APIRouter, Depends, status, HTTPException
from .. import schemas, database, oauth2
from ..repository import user 
from sqlalchemy.orm import Session
from typing import Optional


get_db = database.get_db

router = APIRouter(
    prefix="/user",
    tags=['Users']
)


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_user(request: schemas.User,db: Session = Depends(get_db)):
    return user.create(request,db)


@router.get('/',response_model=schemas.ShowUser)
def get_current_user_data(current_user: schemas.User = Depends(oauth2.get_current_user),db: Session = Depends(get_db)):
    return user.show(current_user,db)


@router.put('/changepassword', status_code=status.HTTP_202_ACCEPTED)
def change_password(request: schemas.ChangePassword,current_user: schemas.TokenData = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return user.change_password( request,current_user, db)


@router.put('/updateuserdata', status_code=status.HTTP_202_ACCEPTED)
def update_user_data(request: schemas.UpdateUserData,current_user: schemas.TokenData = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return user.update_user_data(request,current_user, db)


@router.delete('/', status_code=status.HTTP_204_NO_CONTENT)
def delete(current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return user.delete_account(current_user, db)    

@router.post('/connectaccounts', status_code=status.HTTP_201_CREATED)
def connect_accounts(seller_code: str,current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return user.connect_account(seller_code,current_user, db)

@router.get('/getclients')
def get_all_clients(current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return user.get_all_clients(current_user,db)