from fastapi import APIRouter
from .. import database


router = APIRouter(
    prefix="/health",
    tags=['Health']
)


@router.get('')
def health_all():
    is_database_working = True
    output = 'database is ok'

    try:
        # to check database we will execute raw query
        session = database.get_db.get_database_session()
        session.execute('SELECT 1')
    except Exception as e:
        output = str(e)
        is_database_working = False

    return {'app': 'I\'m working', 'db': output}

@router.get('/app')
def health_app():
    return {'app': 'I\'m working'}

@router.get('/database')
def health_database():
    return {'db': 'Not implemented'}