from fastapi import APIRouter, Depends, status, HTTPException
from .. import schemas, database, oauth2
from ..repository import positions 
from sqlalchemy.orm import Session
from typing import List


get_db = database.get_db

router = APIRouter(
    prefix="/position",
    tags=['Positions']
)


@router.post('/{reckoningnumber}', status_code=status.HTTP_201_CREATED)
def create(reckoningnumber:str, request: schemas.Position,current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return positions.add(reckoningnumber, request,current_user, db)


@router.delete('/{reckoningnumber}/{position_id}', status_code=status.HTTP_204_NO_CONTENT)
def delete(reckoningnumber: str, position_id: int,current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return positions.destroy(reckoningnumber, position_id,current_user, db)


@router.put('/{reckoningnumber}/{position_id}', status_code=status.HTTP_202_ACCEPTED)
def update(reckoningnumber: str, position_id: int, request: schemas.UpdatePosition,current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return positions.update(reckoningnumber, position_id,request,current_user, db)


@router.get('/{reckoningnumber}/{position_id}', response_model=schemas.Position, status_code=status.HTTP_200_OK)
def show(reckoningnumber: str, position_id: int,current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return positions.show(reckoningnumber, position_id,current_user, db)


@router.get('/{reckoningnumber}',response_model=List[schemas.Position], status_code=status.HTTP_200_OK)
def all(reckoningnumber: str,current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return positions.get_all(reckoningnumber,current_user,db)