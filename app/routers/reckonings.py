from fastapi import APIRouter, Depends, status, HTTPException
from .. import schemas, database, oauth2
from sqlalchemy.orm import Session
from ..repository import reckonings 
from typing import List


get_db = database.get_db

router = APIRouter(
    prefix="/reckonings",
    tags=['Reckonings']
)


@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: schemas.CreateReckoning,current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return reckonings.create(request,current_user, db)


@router.delete('/{reckoning_number}', status_code=status.HTTP_204_NO_CONTENT)
def delete(reckoning_number: str,current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return reckonings.destroy(reckoning_number,current_user, db)


@router.put('/', status_code=status.HTTP_202_ACCEPTED)
def update(reckoning_number: str, request: schemas.UpdateReckoning,current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return reckonings.update(reckoning_number,request, current_user, db)


@router.get('/',response_model=List[schemas.ReckoningResponse], status_code=status.HTTP_200_OK)
def all(db: Session = Depends(get_db), current_user: schemas.User = Depends(oauth2.get_current_user)):
    return reckonings.get_all(current_user, db)


@router.get('/{reckoning_number}',response_model=schemas.ReckoningResponse, status_code=status.HTTP_200_OK)
def show(reckoning_number: str,current_user: schemas.User = Depends(oauth2.get_current_user), db: Session = Depends(get_db)):
    return reckonings.show(reckoning_number,current_user, db)
