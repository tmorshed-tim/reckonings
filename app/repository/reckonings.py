from pydantic.typing import NoneType
from sqlalchemy.orm import Session
from sqlalchemy.sql.elements import Null
from .. import models, schemas
from fastapi import HTTPException,status
from ..repository import buyers, sellers


def get_all(current_user: schemas.User, db: Session):
    if current_user.role == "Seller":
        reckonings = db.query(models.Reckoning).join(models.Seller).filter(models.Seller.uuid == current_user.uuid).all()
    elif current_user.role == "Client":
        reckonings = db.query(models.Reckoning).join(models.Buyer).filter(models.Buyer.uuid == current_user.uuid).all()
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Wrong user role")
    if not reckonings:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No reckonings for this user")     
    return reckonings


def show(reckoning_number:str,current_user: schemas.User, db:Session):
    
    if current_user.role == "Seller":
        reckoning = db.query(models.Reckoning).filter(models.Reckoning.number == reckoning_number).filter(models.Seller.uuid == current_user.uuid).first()
    elif current_user.role == "Client":
        reckoning = db.query(models.Reckoning).filter(models.Reckoning.number == reckoning_number).filter(models.Buyer.uuid == current_user.uuid).first()
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Wrong user role")
    if not reckoning:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Reckoning with the number {reckoning_number} does not exist")

    return reckoning

    
def create(request: schemas.UpdateReckoning,current_user: schemas.User,db: Session):
    
    if current_user.role == "Seller":
        reckonings = db.query(models.Reckoning).filter(models.Seller.uuid == current_user.uuid).all()
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Wrong user role")

    for item in reckonings:
        if item.number == request.number:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Reckoning name {request.number} already exist")

    new_buyer = buyers.create(request.buyer, db)
    new_seller = sellers.create(request.seller,current_user.uuid, db)
    new_reckoning = models.Reckoning(number = request.number, buyer_id=new_buyer.id, seller_id=new_seller.id, 
                    place_of_sale = request.place_of_sale, date_of_issue=request.date_of_issue, 
                    date_of_payment=request.date_of_payment, date_sale=request.date_sale,
                    status=request.status, comment=request.comment,
                    person_issuer=request.person_issuer, person_buyer=request.person_buyer)
    db.add(new_reckoning)
    db.commit()
    db.refresh(new_reckoning)
    return new_reckoning


def update(reckoning_number:str,request:schemas.UpdateReckoning,current_user: schemas.User, db:Session):
    
    if current_user.role == "Seller":
        reckoning = db.query(models.Reckoning).filter(models.Reckoning.number == reckoning_number).filter(models.Seller.uuid==current_user.uuid).first()
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Wrong user role")

    if not reckoning:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Reckoning with number {reckoning_number} not found")
    buyer = db.query(models.Buyer).filter(models.Buyer.id == reckoning.buyer_id).first()
    seller = db.query(models.Seller).filter(models.Seller.id == reckoning.seller_id).first()

    if request.buyer:
        if request.buyer.name is not None:
            buyer.name = request.buyer.name
        if request.buyer.address is not None:
            buyer.address = request.buyer.address
        if request.buyer.name is not None:
            buyer.name = request.buyer.name

    if request.seller:
        if request.seller.name is not None:
            seller.name = request.seller.name
        if request.seller.address is not None:
            seller.address = request.seller.address

    if request.place_of_sale is not None:
        reckoning.place_of_sale = request.place_of_sale
    if request.date_of_issue is not None:
        reckoning.date_of_issue = request.date_of_issue
    if request.date_of_payment is not None:
        reckoning.date_of_payment = request.date_of_payment
    if request.date_sale is not None:
        reckoning.date_sale = request.date_sale
    if request.status is not None:
        reckoning.status = request.status
    if request.comment is not None:
        reckoning.comment = request.comment
    if request.person_issuer is not None:
        reckoning.person_issuer = request.person_issuer
    if request.person_buyer is not None:
        reckoning.person_buyer = request.person_buyer

    db.commit()
    return 'updated'


def destroy(reckoning_number:str,current_user: schemas.User,db: Session):

    if current_user.role == "Seller":
        reckoning = db.query(models.Reckoning).filter(models.Reckoning.number == reckoning_number).filter(models.Seller.uuid==current_user.uuid).first()
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Wrong user role")
    if not reckoning:
       raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                           detail=f"Reckoning with number {reckoning_number} not found")

    buyer = db.query(models.Buyer).filter(models.Buyer.id == reckoning.buyer_id).first()
    seller = db.query(models.Seller).filter(models.Seller.id == reckoning.seller_id).first()
    positions = db.query(models.Position).filter(models.Seller.uuid == current_user.uuid).filter(models.Reckoning.number == reckoning_number).all()
    
    for position in positions:
        db.delete(position)
    
    db.delete(reckoning)
    db.delete(seller)
    db.delete(buyer)
    db.commit()
    return 'done'