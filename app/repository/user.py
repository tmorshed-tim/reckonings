import uuid
from sqlalchemy.orm import Session
from sqlalchemy.sql.functions import user
from .. import models, schemas
from fastapi import HTTPException,status
from ..hashing import Hash
from typing import Optional
from random import randint

def create(request: schemas.User,db:Session):
    user_login = db.query(models.User).filter(models.User.login == request.login).first()
    user_email = db.query(models.User).filter(models.User.email == request.email).first()

    if user_login:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=f"Login {request.login} is not available")
    elif user_email:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=f"Email {request.email} is not available")
    else:
        new_user = models.User(name=request.name,email=request.email,
                address=request.address, login = request.login,
                password=Hash.bcrypt(request.password), role = request.role, uuid = str(uuid.uuid4()), add_code = str(randint(1000000000, 9999999999)))
        db.add(new_user)
        db.commit()
        db.refresh(new_user)
    return new_user

def show(current_user: schemas.TokenData,db:Session):
    user = db.query(models.User).filter(models.User.uuid == current_user.uuid).first()
    
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with the uuid {uuid} is not available")
    return user

#TODO - check this
def change_password(request:schemas.ChangePassword,current_user: schemas.TokenData, db:Session):
    user = db.query(models.User).filter(models.User.uuid == current_user.uuid).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User not found")

    if not Hash.verify(user.password, request.previous_password):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Invalid Credentials")
    if not request.new_password == request.verify_password:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Invalid Credentials")

    user.password = Hash.bcrypt(request.new_password)
    db.commit()
    return 'updated'


def update_user_data(request:schemas.UpdateUserData,current_user: schemas.TokenData, db:Session):
    user = db.query(models.User).filter(models.User.uuid == current_user.uuid).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User not found")
    
    if request.name is not None or request.name != '':
        user.name = request.name
    if request.address is not None or request.address != '':
        user.address = request.address

    db.commit()
    return 'updated'

def delete_account(current_user: schemas.User,db: Session):
    user = db.query(models.User).filter(models.User.uuid==current_user.uuid).first()
    
    if not user:
       raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                           detail=f"User not found")

    db.delete(user)
    db.commit()
    return 'done'

def connect_account(seller_code:str, current_user: schemas.User,db: Session):
    if current_user.role == 'Client':
        buyer = db.query(models.User).filter(models.User.uuid == current_user.uuid).first()
        seller = db.query(models.User).filter(models.User.add_code == seller_code).first()

        if not seller:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                           detail=f"Wrong buyer code")

        new_connection = models.UserConnection(buyer_id=buyer.id, seller_id=seller.id)
        db.add(new_connection)
        db.commit()
        db.refresh(new_connection)
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                           detail=f"Wrong user role")
    return 'Added'

def get_all_clients(current_user: schemas.User,db: Session):
    if current_user.role == 'Seller':
        seller = db.query(models.User).filter(models.User.uuid == current_user.uuid).first()
        users_id = db.query(models.UserConnection).filter(models.UserConnection.seller_id == seller.id).all()

        users=[]
        if users.count == 0:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                           detail=f"No connected users")

        for item in users_id:
            user = db.query(models.User).filter(models.User.id == item.id).first()
            users.append({'uuid': user.uuid, 'name':user.name, 'address':user.address})
        print(users)
    return users