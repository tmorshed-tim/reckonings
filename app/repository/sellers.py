from sqlalchemy.orm import Session
from .. import models, schemas
from fastapi import HTTPException,status
import uuid

def create(request: schemas.Seller,uuid,db:Session):
    new_seller = models.Seller(name=request.name,address=request.address, uuid = uuid)
    db.add(new_seller)
    db.commit()
    db.refresh(new_seller)
    return new_seller
