from sqlalchemy.orm import Session
from .. import models, schemas
from fastapi import HTTPException,status


def get_all(reckoningnumber:str,current_user: schemas.User, db: Session):
    if current_user.role == "Seller":
        positions = db.query(models.Position).filter(models.Seller.uuid == current_user.uuid).filter(models.Reckoning.number == reckoningnumber).all()
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Wrong role")
    return positions


def show(reckoningnumber:str, position_id:int,current_user: schemas.User,db:Session):
    if current_user.role == "Seller":
        position = db.query(models.Position).filter(models.Seller.uuid == current_user.uuid) \
        .filter(models.Reckoning.number == reckoningnumber).filter(models.Position.position_id == position_id).first()
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Wrong role")
    if not position:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Position with the id {position_id} is not available")
    
    return position

def add(reckoningnumber:str, request: schemas.Position,current_user: schemas.User, db: Session):
    if current_user.role == "Seller":
        reckonings = db.query(models.Reckoning).filter(models.Seller.uuid == current_user.uuid).filter(models.Reckoning.number == reckoningnumber).first()
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Wrong role")
    
    #TODO - add autoincrement position_id
    current_positions = db.query(models.Position).filter(models.Seller.uuid == current_user.uuid).filter(models.Reckoning.number == reckoningnumber).all()

    for item in current_positions:
        if item.position_id == request.position_id:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Position id {request.position_id} already exist")

    new_position = models.Position(reckoning_id=reckonings.reckoning_id, 
                        position_id=request.position_id, name=request.name, 
                        quantity=request.quantity, unit=request.unit, 
                        price=request.price, tax=request.tax)

    db.add(new_position)
    db.commit()
    db.refresh(new_position)
    return new_position


def update(reckoningnumber:str, position_id:int,request:schemas.UpdatePosition,current_user: schemas.User, db:Session):
    if current_user.role == "Seller":
        position = db.query(models.Position).filter(models.Seller.uuid == current_user.uuid) \
        .filter(models.Reckoning.number == reckoningnumber).filter(models.Position.position_id == position_id).first()
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Wrong role")
    if not position:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Position with the id {position_id} is not available")

    if request.name is not None:
        position.name = request.name
    if request.quantity is not None:
        position.quantity = request.quantity
    if request.unit is not None:
        position.unit = request.unit
    if request.price is not None:
        position.price = request.price
    if request.tax is not None:
        position.tax = request.tax

    db.commit()
    return 'updated'


def destroy(reckoningnumber:str, position_id:int,current_user: schemas.User,db: Session):
    if current_user.role == "Seller":
        position = db.query(models.Position).filter(models.Seller.uuid == current_user.uuid) \
        .filter(models.Reckoning.number == reckoningnumber).filter(models.Position.position_id == position_id).first()
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Wrong role")
    if not position:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Position with the id {position_id} is not available")

    db.delete(position)
    db.commit()
    return 'done'