from sqlalchemy.orm import Session
from .. import models, schemas
from fastapi import HTTPException,status
import uuid

def create(request: schemas.Buyer,db:Session):
    new_buyer = models.Buyer(name=request.name,address=request.address, uuid = request.uuid)
    db.add(new_buyer)
    db.commit()
    db.refresh(new_buyer)
    return new_buyer
