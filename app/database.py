from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

#SQLALCHAMY_DATABASE_URL = 'sqlite:///app/database.db'
SQLALCHAMY_DATABASE_URL = 'postgresql+psycopg2://postgres:stro0nkhasIo@207.180.230.226:5432/tim'

engine = create_engine(SQLALCHAMY_DATABASE_URL)


SessionLocal = sessionmaker(bind=engine)

Base = declarative_base()

def get_db():
    db = SessionLocal()

    try:
        yield db
    finally:
        db.close()