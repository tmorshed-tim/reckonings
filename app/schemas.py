from typing import List, Optional
from pydantic import BaseModel, validator, ValidationError, constr


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    login:str
    email:str
    role:str
    uuid:str


class User(BaseModel): 
    name:Optional[str]
    address: Optional[str]
    login:str
    email:str
    password:str
    role:str
    @validator('name','address','login','email','password','role')
    def cannot_be_empty(cls, v):
        if not v:
            raise ValueError('cannot be empty')
        return v

class UpdateUserData(BaseModel):
    name:Optional[str]
    address: Optional[str]
    @validator('name','address')
    def cannot_be_empty(cls, v):
        if not v:
            raise ValueError('cannot be empty')
        return v

class ShowUser(BaseModel):
    name:str
    address: str
    uuid: str
    class Config():
        orm_mode = True
        

class Position(BaseModel):
    position_id: int
    name: str
    quantity: int
    unit: str
    price: float
    tax: float
    class Config():
        orm_mode = True

class UpdatePosition(BaseModel):
    name: Optional[str]
    quantity: Optional[int]
    unit: Optional[str]
    price: Optional[float]
    tax: Optional[float]
    class Config():
        orm_mode = True

class ChangePassword(BaseModel):
    previous_password: str
    new_password: str
    verify_password: str
    class Config():
        orm_mode = True


class ReckoningBuyer(BaseModel):
    name:str
    address:str
    uuid:str
    class Config():
        orm_mode = True
    @validator('name','address','uuid')
    def cannot_be_empty(cls, v):
        if not v:
            raise ValueError('cannot be empty')
        return v
    

class UpdateReckoningBuyer(BaseModel):
    name:Optional[str]
    address:Optional[str]
    uuid:Optional[str]
    class Config():
        orm_mode = True
    @validator('name','address','uuid')
    def cannot_be_empty(cls, v):
        if not v:
            raise ValueError('cannot be empty')
        return v
    @validator('uuid')
    def date_validator(cls, v):
        if len(v) != 36:
            raise ValueError('must be exacly 36')
        return v

class Buyer(BaseModel):
    id: int
    name:str
    address:str
    uuid:str
    class Config():
        orm_mode = True

class ReckoningSeller(BaseModel):
    name:str
    address:str
    class Config():
        orm_mode = True
    @validator('name','address')
    def cannot_be_empty(cls, v):
        if not v:
            raise ValueError('cannot be empty')
        return v

class UpdateReckoningSeller(BaseModel):
    name:Optional[str]
    address:Optional[str]
    class Config():
        orm_mode = True
    @validator('name','address')
    def cannot_be_empty(cls, v):
        if not v:
            raise ValueError('cannot be empty')
        return v.title()

class Seller(BaseModel):
    id: int
    name:str
    address:str
    uuid:str
    class Config():
        orm_mode = True

class CreateReckoning(BaseModel):
    number: str
    buyer: ReckoningBuyer
    seller: ReckoningSeller
    place_of_sale: str
    date_of_issue: str
    date_of_payment: str
    date_sale: str
    status: str
    #positions: List[Position]
    comment: str
    person_issuer: str
    person_buyer: str
    class Config():
        orm_mode = True

    @validator('number','place_of_sale','date_of_issue','date_of_payment','date_sale','status','person_issuer','person_buyer')
    def cannot_be_empty(cls, v):
        if not v:
            raise ValueError('cannot be empty')
        return v
    
    @validator('number')
    def number_without_space_or_slash(cls, v):
        if ' ' in v or '/' in v:
            raise ValueError('cannot contain a space or /')
        return v
    
    @validator('date_sale', 'date_of_issue', 'date_of_payment')
    def date_validator(cls, v):
        if len(v) < 8:
            raise ValueError('must be longer than 7')
        return v




class UpdateReckoning(BaseModel):
    buyer: Optional[UpdateReckoningBuyer]
    seller: Optional[UpdateReckoningSeller]
    place_of_sale: Optional[str]
    date_of_issue: Optional[str]
    date_of_payment: Optional[str]
    date_sale: Optional[str]
    status: Optional[str]
    positions: List[Position] = []
    comment: Optional[str]
    person_issuer: Optional[str]
    person_buyer: Optional[str]
    class Config():
        orm_mode = True
    @validator('place_of_sale','date_of_issue','date_of_payment','date_sale','status','person_issuer','person_buyer')
    def cannot_be_empty(cls, v):
        if not v:
            raise ValueError('cannot be empty')
        return v

    @validator('date_sale', 'date_of_issue', 'date_of_payment')
    def date_validator(cls, v):
        if len(v) < 8:
            raise ValueError('must be longer than 7')
        return v

    
class ReckoningResponse(BaseModel):
    reckoning_id: int
    number: str
    buyer: Buyer
    seller: Seller
    place_of_sale: str
    date_of_issue: str
    date_of_payment: str
    date_sale: str
    status: str
    positions: List[Position] = []
    comment: str
    person_issuer: str
    person_buyer: str
    class Config():
        orm_mode = True 
