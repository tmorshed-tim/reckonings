from enum import Enum

class ReckoningStatus(Enum):
    NEW = 'New',
    PAID = 'Paid',
    WAITING_FOR_PAYMENT = 'Waiting for payment'

    def __str__(self):
        return '%s' % self.value


def change_status(current, next_value):
    if (str(current)==ReckoningStatus.NEW & str(next_value)==ReckoningStatus.PAID):
        return True
    elif (str(current)==ReckoningStatus.NEW & str(next_value)==ReckoningStatus.WAITING_FOR_PAYMENT):
        return True
    elif str(current)==ReckoningStatus.WAITING_FOR_PAYMENT & str(next_value)==ReckoningStatus.PAID):
        return True
    else:
        return False