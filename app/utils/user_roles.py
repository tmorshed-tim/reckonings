from enum import Enum

class ReckoningStatus(Enum):
    CLIENT = 'Client',
    SELLER = 'Seller',
    ADMIN = 'Admin'

    def __str__(self):
        return '%s' % self.value