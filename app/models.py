from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.sql.sqltypes import Float
from .database import Base
from sqlalchemy.orm import relationship


class Buyer(Base):
    __tablename__ = 'buyers'
    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String)
    name = Column(String)
    address = Column(String)
    reckoning_buyer = relationship('Reckoning', uselist=False, back_populates="buyer")


class Seller(Base):
    __tablename__ = 'sellers'
    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String)
    name = Column(String)
    address = Column(String)
    reckoning_seller = relationship('Reckoning', uselist=False, back_populates="seller")


class Reckoning(Base):
    __tablename__ = 'reckonings'

    reckoning_id = Column(Integer, primary_key=True, index=True)
    number = Column(String)
    buyer_id = Column(Integer, ForeignKey('buyers.id'))
    buyer = relationship("Buyer",uselist=False, back_populates="reckoning_buyer")
    seller_id = Column(Integer, ForeignKey('sellers.id'))
    seller = relationship("Seller",uselist=False, back_populates="reckoning_seller")
    place_of_sale = Column(String)
    date_of_issue = Column(String)
    date_of_payment = Column(String)
    date_sale = Column(String)
    status = Column(String)
    comment = Column(String)
    person_issuer = Column(String)
    person_buyer = Column(String)

    positions = relationship('Position', backref='reckoning')


class Position(Base):
    __tablename__ = 'positions'

    id = Column(Integer, primary_key=True, index=True)
    reckoning_id = Column(Integer, ForeignKey('reckonings.reckoning_id'))
 
    position_id = Column(Integer)
    name = Column(String)
    quantity = Column(Integer)
    unit = Column(String)
    price = Column(Float)
    tax = Column(Float)



class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    address = Column(String)
    login = Column(String, unique=True)
    email = Column(String, unique=True)
    password = Column(String)
    role = Column(String)
    uuid = Column(String)
    add_code = Column(String)


class UserConnection(Base):
    __tablename__ = 'users_connections'
    id = Column(Integer, primary_key=True, index=True)
    buyer_id = Column(Integer, ForeignKey('users.id'))
    seller_id = Column(Integer, ForeignKey('users.id'))
 