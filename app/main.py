from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.routers import positions, reckonings, user, authentication, health
from .database import engine
from . import  models

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:4200",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

models.Base.metadata.create_all(engine)

app.include_router(reckonings.router)
app.include_router(positions.router)
app.include_router(user.router)
app.include_router(authentication.router)
app.include_router(health.router)