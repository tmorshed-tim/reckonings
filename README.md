# Reckonings

# How to run local project

## 1. Create enviroment
```
python3 -m venv fastapi-env
```

## 2. Activate enviroment

### Windows
```
fastapi-env\Scripts\activate.bat
```

### Unix / MacOS
```
source fastapi-env/bin/activate
```

## 3. Install requirements 
```
pip3 install -r requirements.txt
```

## 4. (Optional) Install fastapi
```
pip3 install fastapi
```

## 5. (Optional) Instal local server
```
pip3 install uvicorn
```

## 6. Run server
```
python3 -m uvicorn app.main:app --reload
```

# How to run project using docker
 ## 1. Build docker container
```
 docker build -t reckonings_backend .
 ```

 ## 2. Run docker Container
 ```
 docker run -d --name reckonings_backend -p 8000:8000 reckonings_backend
 ```


# Project documentation
Interactive:
 http://127.0.0.1:8000/docs

Alternative API docs:
 http://127.0.0.1:8000/redoc

 # FastAPI docs
 https://fastapi.tiangolo.com


# Postgres database container 
```
docker run -d \
    --name some-postgres \
    -e POSTGRES_PASSWORD=stro0nkhasIo \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v /custom/mount:/var/lib/postgresql/data \
    -p 5432:5432 \
    postgres
``` 